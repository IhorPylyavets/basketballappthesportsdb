# TheSportDb (Basketball)

![List Events](/images/Screenshot_1569321150.jpg)
![Team](/images/Screenshot_1569321184.jpg)
![Player](/images/Screenshot_1569321205.jpg)

Kotlin android application example with MVVM pattern, android architecture components, and kotlin coroutine. This sample is about basketball match schedules which the API i used from [TheSportDb](https://www.thesportsdb.com/api.php).

Here are several libraries that I used:

* [Coroutine](https://github.com/Kotlin/kotlinx.coroutines#user-content-android) // Threading

* [ViewModel](https://developer.android.com/topic/libraries/architecture/viewmodel)

* [LiveData](https://developer.android.com/topic/libraries/architecture/livedata)

* [Room](https://developer.android.com/training/data-storage/room/) // Local data storage

* [Retrofit](https://square.github.io/retrofit/) // Networking

* [Picasso](https://square.github.io/picasso/) // Image loader