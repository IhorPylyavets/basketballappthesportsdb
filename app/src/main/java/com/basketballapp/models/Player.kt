package com.basketballapp.models

import androidx.room.Entity
import com.squareup.moshi.Json

@Entity(tableName = "players", primaryKeys = ["idPlayer", "idTeam"])
data class Player(
    @Json(name = "idPlayer")
    val idPlayer: String,

    @Json(name = "idTeam")
    val idTeam: String,

    @Json(name = "strCutout")
    val strCutout: String?,

    @Json(name = "strDescriptionEN")
    val strDescriptionEN: String?,

    @Json(name = "strFanart1")
    val strFanart1: String?,

    @Json(name = "strHeight")
    val strHeight: String?,

    @Json(name = "strPlayer")
    val strPlayer: String?,

    @Json(name = "strPosition")
    val strPosition: String?,

    @Json(name = "strThumb")
    val strThumb: String?,

    @Json(name = "strWeight")
    val strWeight: String?
) {

    fun getWeight(): String = formatNumber(strWeight)
    fun getHeight(): String = formatNumber(strHeight)

    private fun formatNumber(number: String?) : String {
        if (number.isNullOrBlank()) return "-"
        val regex = """[\d]*\.[\d]*""".toRegex().find(number)?.value
        return regex ?: "-"
    }
}