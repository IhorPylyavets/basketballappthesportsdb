package com.basketballapp.models

import android.annotation.SuppressLint
import androidx.room.Entity
import com.basketballapp.features.main.fragments.ListMatchesFragment
import com.squareup.moshi.Json
import java.text.SimpleDateFormat
import java.util.*

@Entity(tableName = "matches", primaryKeys = ["idEvent"])
data class Match(
    @Json(name = "idEvent")
    val idEvent: String,

    @Json(name = "dateEvent")
    val dateEvent: String?,

    @Json(name = "idAwayTeam")
    val idAwayTeam: String?,

    @Json(name = "idHomeTeam")
    val idHomeTeam: String?,

    @Json(name = "idLeague")
    val idLeague: String?,

    @Json(name = "intAwayScore")
    val intAwayScore: String?,

    @Json(name = "intHomeScore")
    val intHomeScore: String?,

    @Json(name = "strAwayTeam")
    val strAwayTeam: String?,

    @Json(name = "strHomeTeam")
    val strHomeTeam: String?,

    @Json(name = "strTime")
    val strTime: String?,

    @Json(name = "strEvent")
    val strEvent: String?,

    @Json(name = "strHomeTeamIcon")
    var strHomeTeamIcon: String?,

    @Json(name = "strAwayTeamIcon")
    var strAwayTeamIcon: String?,

    @Json(name = "strThumb")
    val strThumb: String?,

    @Json(name = "strVideo")
    val strVideo: String?
) {
    var matchType: String? = null

    @SuppressLint("SimpleDateFormat")
    fun getDate() : String {
        dateEvent?.let {
            val pattern = "EEE, d MMM yyyy"
            val dateFormat = SimpleDateFormat("yyyy-MM-dd")
            val date = dateFormat.parse(it)
            dateFormat.applyPattern(pattern)
            return dateFormat.format(date)
        }
        return ""
    }

    @SuppressLint("SimpleDateFormat")
    fun isNextMatch() : Boolean {
        dateEvent?.let {
            val dateFormat = SimpleDateFormat("yyyy-MM-dd")
            val date: Date = dateFormat.parse(it)
            return date.after(Date())
        }
        return false
    }

    fun getStartTime(): Long? {
        if (dateEvent == null || strTime == null) return null
        val time = "$dateEvent $strTime"
        val dateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm")
        val date: Date = dateFormat.parse(time)
        return date.time
    }

    fun format(strHomeGoalDetails: String?): String {
        return strHomeGoalDetails?.replace(";\\s?".toRegex(), "\n") ?: "-"
    }

    fun getHour(): String? {
        strTime?.let {
            val pattern = "HH:mm"
            val dateFormat = SimpleDateFormat("HH:mm")
            val date = dateFormat.parse(it)
            dateFormat.applyPattern(pattern)
            return dateFormat.format(date)
        }
        return ""
    }

    fun defineMatchType(): String {
        val matchTimeL = getStartTime() ?: return ListMatchesFragment.TYPE_PREV_MATCH
        val matchTime = Date(matchTimeL)
        val now = Date()
        return if (matchTime.after(now)) {
            ListMatchesFragment.TYPE_NEXT_MATCH
        } else {
            ListMatchesFragment.TYPE_PREV_MATCH
        }
    }
}