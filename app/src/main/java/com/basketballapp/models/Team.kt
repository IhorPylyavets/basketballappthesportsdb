package com.basketballapp.models

import androidx.room.Entity
import com.squareup.moshi.Json

@Entity(tableName = "teams", primaryKeys = ["idLeague", "idTeam"])
data class Team (
    @Json(name = "idLeague")
    var idLeague: String,

    @Json(name = "idTeam")
    var idTeam: String,

    @Json(name = "strTeamBadge")
    val strTeamBadge: String?,

    @Json(name = "intFormedYear")
    val intFormedYear: String?,

    @Json(name = "strTeam")
    val strTeam: String?,

    @Json(name = "strDescriptionEN")
    val strDescriptionEN: String?,

    @Json(name = "strStadium")
    val strStadium: String?
) {

    fun getFormedYear(): String {
        return "Est. $intFormedYear"
    }
}