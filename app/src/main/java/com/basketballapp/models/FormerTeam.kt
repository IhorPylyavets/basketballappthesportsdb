package com.basketballapp.models

import androidx.room.Entity
import com.squareup.moshi.Json

@Entity(tableName = "former_teams", primaryKeys = ["id"])
data class FormerTeam(
    @Json(name = "id")
    val id: String, //11819

    @Json(name = "idPlayer")
    val idPlayer: String, //34147178

    @Json(name = "idFormerTeam")
    val idFormerTeam: String, //134284

    @Json(name = "strSport")
    val strSport: String, //Soccer

    @Json(name = "strPlayer")
    val strPlayer: String, //Edenilson

    @Json(name = "strFormerTeam")
    val strFormerTeam: String, //Corinthians

    @Json(name = "strTeamBadge")
    val strTeamBadge: String, //https://www.thesportsdb.com/images/media/team/badge/vvuvps1473538042.png

    @Json(name = "strJoined")
    val strJoined: String, //2011

    @Json(name = "strDeparted")
    val strDeparted: String //2014
)