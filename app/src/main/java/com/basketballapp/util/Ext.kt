package com.basketballapp.util

import android.content.Context
import com.basketballapp.R

fun Context.getLeaguesId(position: Int) : String {
    return resources.getStringArray(R.array.leagues_id)[position]
}