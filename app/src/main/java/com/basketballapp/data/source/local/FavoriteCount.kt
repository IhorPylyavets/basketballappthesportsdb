package com.basketballapp.data.source.local

data class FavoriteCount(
    val favCount: Int
)