package com.basketballapp.data.source.remote

import androidx.lifecycle.LiveData
import retrofit2.http.GET
import retrofit2.http.Query

interface BasketballService {

    @GET("eventsnextleague.php")
    fun getNextMatch(@Query("id") leagueId: String) : LiveData<ApiResponse<SchedulesResponse>>

    @GET("eventspastleague.php")
    fun getLastMatch(@Query("id") leagueId: String) : LiveData<ApiResponse<SchedulesResponse>>

    @GET("lookupteam.php")
    fun getTeam(@Query("id") teamId: String) : LiveData<ApiResponse<TeamsResponse>>

    @GET("lookupevent.php")
    fun getMatchDetail(@Query("id") matchId: String) : LiveData<ApiResponse<SchedulesResponse>>

    @GET("lookup_all_teams.php")
    fun getTeams(@Query("id") leagueId: String) : LiveData<ApiResponse<TeamsResponse>>

    @GET("lookup_all_players.php")
    fun getPlayers(@Query("id") teamId: String) : LiveData<ApiResponse<PlayersResponse>>

    @GET("lookupplayer.php")
    fun getPlayer(@Query("id") playerId: String): LiveData<ApiResponse<PlayersResponse>>

    @GET("lookupformerteams.php")
    fun getPlayerFormerTeams(@Query("id") playerId: String): LiveData<ApiResponse<PlayerFormerTeamsResponse>>

    @GET("searchevents.php")
    fun searchMatch(@Query("e") query: String): LiveData<ApiResponse<SearchSchedulesResponse>>

    @GET("searchteams.php")
    fun searchTeam(@Query("t") query: String): LiveData<ApiResponse<TeamsResponse>>

}