package com.basketballapp.data.source.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.basketballapp.models.*

@Database(entities = [Match::class, Team::class, FavoriteMatch::class, FavoriteTeam::class,
    Player::class, FavoritePlayer::class, FormerTeam::class], version = 1)
abstract class BasketballDatabase : RoomDatabase() {
    abstract fun matchesDao(): MatchesDao
    abstract fun teamsDao(): TeamsDao
    abstract fun playersDao(): PlayersDao

    companion object {

        @Volatile
        private var INSTANCE: BasketballDatabase? = null

        fun getDatabase(context: Context): BasketballDatabase {
            return INSTANCE ?: synchronized(this) {
                Room.databaseBuilder(context.applicationContext, BasketballDatabase::class.java, "basketball_db")
                    .fallbackToDestructiveMigration()
                    .build()
                    .also { INSTANCE = it }
            }
        }
    }
}