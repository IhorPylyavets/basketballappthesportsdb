package com.basketballapp.data.source

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import com.basketballapp.ContextProviders
import com.basketballapp.data.source.local.*
import com.basketballapp.data.source.remote.*
import com.basketballapp.features.main.fragments.ListMatchesFragment
import com.basketballapp.models.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class BasketballRepository(
    private val db: BasketballDatabase,
    private val matchesDao: MatchesDao,
    private val teamsDao: TeamsDao,
    private val playersDao: PlayersDao,
    private val basketballService: BasketballService,
    private val coroutineContext: ContextProviders) {

    companion object {
        private var INSTANCE: BasketballRepository? = null

        fun getInstance(basketballDatabase: BasketballDatabase, basketballService: BasketballService)
                : BasketballRepository = INSTANCE ?: synchronized(BasketballRepository::class.java) {
            BasketballRepository(
                basketballDatabase,
                basketballDatabase.matchesDao(),
                basketballDatabase.teamsDao(),
                basketballDatabase.playersDao(),
                basketballService,
                ContextProviders.getInstance()
            ).also { INSTANCE = it }
        }
    }

    fun nextMatches(leagueId: String): LiveData<Resource<List<Match>>> {
        return object : NetworkBoundResource<List<Match>, SchedulesResponse>(coroutineContext) {
            override fun saveCallResult(item: SchedulesResponse) {
                val matches = item.events
                matches?.let { matchesData ->
                    matchesData.forEach { match ->
                        match?.let {
                            match.matchType = ListMatchesFragment.TYPE_NEXT_MATCH
                        }
                    }

                    db.runInTransaction {
                        matchesDao.deleteNextMatches(leagueId)
                        matchesDao.saveMatches(matchesData)
                    }
                }
            }

            override fun createCall(): LiveData<ApiResponse<SchedulesResponse>> = basketballService.getNextMatch(leagueId)

            override fun shouldFetch(data: List<Match>?) = true

            override fun loadFromDb(): LiveData<List<Match>> = matchesDao.getNextMatches(leagueId)

        }.asLiveData()
    }

    fun prevMatches(leagueId: String): LiveData<Resource<List<Match>>> {
        return object : NetworkBoundResource<List<Match>, SchedulesResponse>(coroutineContext) {

            override fun saveCallResult(item: SchedulesResponse) {
                val matches = item.events
                matches?.let { matchesData ->
                    matchesData.forEach { match ->
                        match?.let {
                            match.matchType = ListMatchesFragment.TYPE_PREV_MATCH
                        }
                    }

                    db.runInTransaction {
                        matchesDao.deletePrevMatches(leagueId)
                        matchesDao.saveMatches(matchesData)
                    }
                }
            }

            override fun createCall(): LiveData<ApiResponse<SchedulesResponse>> = basketballService.getLastMatch(leagueId)

            override fun shouldFetch(data: List<Match>?): Boolean = true

            override fun loadFromDb(): LiveData<List<Match>> = matchesDao.getPrevMatches(leagueId)

        }.asLiveData()
    }

    fun getMatchEventDetail(matchId: String): LiveData<Resource<Match>> {
        return object : NetworkBoundResource<Match, SchedulesResponse>(coroutineContext) {
            override fun saveCallResult(item: SchedulesResponse) {
                item.events?.let { matches ->
                    matches.forEach { match ->
                        match?.let {
                            if (match.isNextMatch()) {
                                match.matchType = ListMatchesFragment.TYPE_NEXT_MATCH
                            } else {
                                match.matchType = ListMatchesFragment.TYPE_PREV_MATCH
                            }
                        }
                    }

                    matchesDao.saveMatches(matches)
                }
            }

            override fun createCall(): LiveData<ApiResponse<SchedulesResponse>> = basketballService.getMatchDetail(matchId)

            override fun shouldFetch(data: Match?): Boolean = data == null

            override fun loadFromDb(): LiveData<Match> = matchesDao.getMatchDetail(matchId)

        }.asLiveData()
    }

    fun isFavoriteMatch(matchId: String): LiveData<Boolean> {
        val isFavorite = MediatorLiveData<Boolean>()
        val favCount = matchesDao.isFavoriteMatch(matchId)

        isFavorite.addSource(favCount) { data ->
            data?.let {
                isFavorite.value = it.favCount > 0
            }
        }
        return isFavorite
    }

    fun toggleFavoriteMatch(matchId: String, isFavorite: Boolean) {
        GlobalScope.launch(Dispatchers.IO) {
            if (isFavorite) { // Remove from favorite
                matchesDao.deleteFavoriteMatch(matchId)
            } else { // Add to favorite
                matchesDao.addToFavoriteMatch(FavoriteMatch(matchId))
            }
        }
    }

    fun getFavoriteMatches(): LiveData<Resource<List<Match>>> {
        val data = MediatorLiveData<Resource<List<Match>>>()
        data.value = Resource.loading(null)
        data.addSource(matchesDao.getFavoriteMatches()) {
            if (it != null) {
                data.value = Resource.success(it)
            }
        }
        return data
    }

    fun getTeam(teamId: String): LiveData<Resource<Team>> {
        return object : NetworkBoundResource<Team, TeamsResponse>(coroutineContext) {
            override fun saveCallResult(item: TeamsResponse) {
                item.teams?.let {
                    db.runInTransaction {
                        teamsDao.saveTeams(it)
                    }
                }
            }

            override fun createCall(): LiveData<ApiResponse<TeamsResponse>> = basketballService.getTeam(teamId)

            override fun shouldFetch(data: Team?): Boolean = data == null

            override fun loadFromDb(): LiveData<Team> = teamsDao.getTeam(teamId)

        }.asLiveData()
    }

    fun teams(leagueId: String): LiveData<Resource<List<Team>>> {
        return object : NetworkBoundResource<List<Team>, TeamsResponse>(coroutineContext) {

            override fun saveCallResult(item: TeamsResponse) {
                item.teams?.let {
                    db.runInTransaction {
                        teamsDao.saveTeams(it)
                    }
                }
            }

            override fun createCall(): LiveData<ApiResponse<TeamsResponse>> = basketballService.getTeams(leagueId)

            override fun shouldFetch(data: List<Team>?): Boolean = true

            override fun loadFromDb(): LiveData<List<Team>> = teamsDao.getTeams(leagueId)

        }.asLiveData()
    }

    fun toggleFavoriteTeam(teamId: String, isFavorite: Boolean) {
        GlobalScope.launch(Dispatchers.IO) {
            if (isFavorite) { // Remove from favorite
                teamsDao.deleteFavoriteTeam(teamId)
            } else { // Add to favorite
                teamsDao.addToFavoriteTeam(FavoriteTeam(teamId))
            }
        }
    }

    fun isFavoriteTeam(teamId: String): LiveData<Boolean> {
        val isFavorite = MediatorLiveData<Boolean>()
        val favCount = teamsDao.isFavoriteTeam(teamId)

        isFavorite.addSource(favCount) { data ->
            data?.let {
                isFavorite.value = it.favCount > 0
            }
        }
        return isFavorite
    }

    fun getFavoriteTeams(): LiveData<Resource<List<Team>>> {
        val data = MediatorLiveData<Resource<List<Team>>>()
        data.value = Resource.loading(null)
        data.addSource(teamsDao.getFavoriteTeams()) {
            if (it != null) {
                data.value = Resource.success(it)
            }
        }
        return data
    }

    fun getPlayers(teamId: String): LiveData<Resource<List<Player>>> {
        return object : NetworkBoundResource<List<Player>, PlayersResponse>(coroutineContext) {

            override fun saveCallResult(item: PlayersResponse) {
                item.player?.let { players ->
                    playersDao.savePlayers(players)
                }
            }

            override fun createCall(): LiveData<ApiResponse<PlayersResponse>> = basketballService.getPlayers(teamId)

            override fun shouldFetch(data: List<Player>?): Boolean = data?.isEmpty() ?: true

            override fun loadFromDb(): LiveData<List<Player>> = playersDao.getPlayers(teamId)

        }.asLiveData()
    }

    fun getPlayer(playerId: String): LiveData<Resource<Player>> {
        return object : NetworkBoundResource<Player, PlayersResponse>(coroutineContext) {
            override fun saveCallResult(item: PlayersResponse) {
                val player = item.player?.get(0)
                player?.let {
                    playersDao.savePlayers(item.player)
                }
            }

            override fun createCall(): LiveData<ApiResponse<PlayersResponse>> = basketballService.getPlayer(playerId)

            override fun shouldFetch(data: Player?): Boolean = true

            override fun loadFromDb(): LiveData<Player> = playersDao.getPlayer(playerId)

        }.asLiveData()
    }

    fun isFavoritePlayer(playerId: String): LiveData<Boolean> {
        val isFavorite = MediatorLiveData<Boolean>()
        val favCount = playersDao.isFavoritePlayer(playerId)

        isFavorite.addSource(favCount) { data ->
            data?.let {
                isFavorite.value = it.favCount > 0
            }
        }
        return isFavorite
    }

    fun toggleFavoritePlayer(playerId: String, isFavorite: Boolean) {
        GlobalScope.launch(Dispatchers.IO) {
            if (isFavorite) { // Remove from favorite
                playersDao.deleteFavoritePlayer(playerId)
            } else { // Add to favorite
                playersDao.addToFavoritePlayer(FavoritePlayer(playerId))
            }
        }
    }

    fun getFavoritePlayers(): LiveData<Resource<List<Player>>> {
        val data = MediatorLiveData<Resource<List<Player>>>()
        data.value = Resource.loading(null)
        data.addSource(playersDao.getFavoritePlayers()) {
            if (it != null) {
                data.value = Resource.success(it)
            }
        }
        return data
    }

    fun getFormerTeams(playerId: String): LiveData<Resource<List<FormerTeam>>> {
        return object : NetworkBoundResource<List<FormerTeam>, PlayerFormerTeamsResponse>(coroutineContext) {
            override fun saveCallResult(item: PlayerFormerTeamsResponse) {
                val player = item.formerteams?.get(0)
                player?.let {
                    teamsDao.saveFormerTeams(item.formerteams)
                }
            }

            override fun createCall(): LiveData<ApiResponse<PlayerFormerTeamsResponse>> = basketballService.getPlayerFormerTeams(playerId)

            override fun shouldFetch(data: List<FormerTeam>?): Boolean = true

            override fun loadFromDb(): LiveData<List<FormerTeam>> = teamsDao.getFormerTeams(playerId)

        }.asLiveData()
    }

    fun searchMatch(query: String): LiveData<Resource<List<Match>>> {
        return object : NetworkBoundResource<List<Match>, SearchSchedulesResponse>(coroutineContext) {
            override fun saveCallResult(item: SearchSchedulesResponse) {
                item.event?.let { matches ->
                    matches.forEach { match ->
                        match?.matchType = match?.defineMatchType()
                    }

                    matchesDao.saveMatches(matches)
                }
            }

            override fun createCall(): LiveData<ApiResponse<SearchSchedulesResponse>> = basketballService.searchMatch(query)

            override fun shouldFetch(data: List<Match>?): Boolean = true

            override fun loadFromDb(): LiveData<List<Match>> = matchesDao.searchMatch("%$query%")

        }.asLiveData()
    }

    fun searchTeam(query: String): LiveData<Resource<List<Team>>> {
        return object : NetworkBoundResource<List<Team>, TeamsResponse>(coroutineContext) {
            override fun saveCallResult(item: TeamsResponse) {
                item.teams?.let { teams ->
                    teamsDao.saveTeams(teams)
                }
            }

            override fun createCall(): LiveData<ApiResponse<TeamsResponse>> = basketballService.searchTeam(query)

            override fun shouldFetch(data: List<Team>?): Boolean = true

            override fun loadFromDb(): LiveData<List<Team>> = teamsDao.searchTeam("%$query%")

        }.asLiveData()
    }
}