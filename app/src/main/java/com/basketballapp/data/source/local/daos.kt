package com.basketballapp.data.source.local

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.basketballapp.features.main.fragments.ListMatchesFragment
import com.basketballapp.models.*

@Dao
interface MatchesDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveMatches(matches: List<Match?>)

    @Query("DELETE FROM matches WHERE matchType = '${ListMatchesFragment.TYPE_NEXT_MATCH}' AND idLeague = :idLeague")
    fun deleteNextMatches(idLeague: String?)

    @Query("DELETE FROM matches WHERE matchType = '${ListMatchesFragment.TYPE_PREV_MATCH}' AND idLeague = :idLeague")
    fun deletePrevMatches(idLeague: String?)

    @Query("SELECT * FROM matches WHERE idLeague = :idLeague AND matchType = '${ListMatchesFragment.TYPE_NEXT_MATCH}' ORDER BY dateEvent DESC LIMIT 15")
    fun getNextMatches(idLeague: String?): LiveData<List<Match>>

    @Query("SELECT * FROM matches WHERE idLeague = :idLeague AND matchType = '${ListMatchesFragment.TYPE_PREV_MATCH}' ORDER BY dateEvent DESC LIMIT 15")
    fun getPrevMatches(idLeague: String?): LiveData<List<Match>>

    @Query("SELECT * FROM matches WHERE idEvent = :matchId")
    fun getMatchDetail(matchId: String): LiveData<Match>

    @Query("SELECT COUNT(*) as favCount FROM favorite_matches WHERE idMatch = :idEvent")
    fun isFavoriteMatch(idEvent: String): LiveData<FavoriteCount>

    @Query("DELETE FROM favorite_matches WHERE idMatch = :matchId")
    fun deleteFavoriteMatch(matchId: String)

    @Query("SELECT * FROM matches INNER JOIN favorite_matches ON favorite_matches.idMatch = idEvent ORDER BY dateEvent DESC")
    fun getFavoriteMatches(): LiveData<List<Match>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addToFavoriteMatch(favMatch: FavoriteMatch)

    @Query("SELECT * FROM matches WHERE strEvent LIKE :query ORDER BY dateEvent DESC")
    fun searchMatch(query: String): LiveData<List<Match>>
}

@Dao
interface TeamsDao {
    @Query("SELECT * FROM teams WHERE idTeam = :teamId")
    fun getTeam(teamId: String): LiveData<Team>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveTeams(it: List<Team?>)

    @Query("SELECT * FROM teams WHERE idLeague = :leagueId")
    fun getTeams(leagueId: String): LiveData<List<Team>>

    @Query("SELECT COUNT(*) as favCount FROM favorite_teams WHERE idTeam = :teamId")
    fun isFavoriteTeam(teamId: String): LiveData<FavoriteCount>

    @Query("DELETE FROM favorite_teams WHERE idTeam = :teamId")
    fun deleteFavoriteTeam(teamId: String)

    @Query("SELECT * FROM teams INNER JOIN favorite_teams ON favorite_teams.idTeam = teams.idTeam")
    fun getFavoriteTeams(): LiveData<List<Team>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveFormerTeams(formerTeams: List<FormerTeam?>)

    @Query("SELECT * FROM former_teams WHERE idPlayer = :playerId")
    fun getFormerTeams(playerId: String): LiveData<List<FormerTeam>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addToFavoriteTeam(favoriteTeam: FavoriteTeam)

    @Query("SELECT * FROM teams WHERE strTeam LIKE :query")
    fun searchTeam(query: String): LiveData<List<Team>>
}

@Dao
interface PlayersDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun savePlayers(players: List<Player?>)

    @Query("SELECT * FROM players WHERE idTeam = :teamId")
    fun getPlayers(teamId: String): LiveData<List<Player>>

    @Query("SELECT * FROM players WHERE idPlayer = :playerId")
    fun getPlayer(playerId: String): LiveData<Player>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addToFavoritePlayer(favoritePlayer: FavoritePlayer)

    @Query("SELECT COUNT(*) as favCount FROM favorite_players WHERE idPlayer = :playerId")
    fun isFavoritePlayer(playerId: String): LiveData<FavoriteCount>

    @Query("DELETE FROM favorite_players WHERE idPlayer = :playerId")
    fun deleteFavoritePlayer(playerId: String)

    @Query("SELECT * FROM players INNER JOIN favorite_players ON favorite_players.idPlayer = players.idPlayer")
    fun getFavoritePlayers(): LiveData<List<Player>>
}