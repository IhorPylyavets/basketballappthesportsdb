package com.basketballapp.data.source.remote

import com.basketballapp.models.*
import com.squareup.moshi.Json

data class SchedulesResponse(
    @Json(name = "events")
    val events: List<Match?>?
)

data class TeamsResponse (
    @Json(name = "teams")
    val teams: List<Team?>?
)

data class PlayersResponse(
    @Json(name = "player")
    val player: List<Player?>?
)

data class PlayerFormerTeamsResponse(
    @Json(name = "formerteams")
    val formerteams: List<FormerTeam?>?
)

data class SearchSchedulesResponse (
    @Json(name = "event")
    val event: List<Match?>?
)