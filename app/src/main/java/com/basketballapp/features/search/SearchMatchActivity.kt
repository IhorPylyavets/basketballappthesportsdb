package com.basketballapp.features.search

import androidx.lifecycle.Observer
import com.basketballapp.custom.BaseSearchActivity
import com.basketballapp.features.main.adapter.MatchesAdapter
import com.basketballapp.features.matchdetail.MatchDetailActivity
import com.basketballapp.models.Match
import com.basketballapp.models.Resource
import kotlinx.android.synthetic.main.list_items.*
import com.basketballapp.util.obtainViewModel

class SearchMatchActivity : BaseSearchActivity<Match>() {

    private lateinit var viewModel: SearchViewModel

    override fun setupAdapter() {
        recycler_view.adapter = MatchesAdapter(this, Resource.loading(null)) {
            startActivity(MatchDetailActivity.getStartIntent(this, it.idEvent, it.idHomeTeam, it.idAwayTeam))
        }
    }

    override fun setupData() {
        viewModel = obtainViewModel()
        with(viewModel) {
            submitQuery(query)
            resultMatches.observe(this@SearchMatchActivity, Observer { data -> updateData(data) })
        }
    }

    override fun submitQuery(query: String?) {
        viewModel.submitQuery(query)
    }

    override fun updateData(data: Resource<List<Match>>?) {
        if (data == null || recycler_view == null) return
        (recycler_view.adapter as MatchesAdapter).submitData(data)
    }

    private fun obtainViewModel() = obtainViewModel(SearchViewModel::class.java)
}