package com.basketballapp.features.search

import android.app.Application
import android.content.Context
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.basketballapp.data.source.BasketballRepository
import com.basketballapp.models.Match
import com.basketballapp.models.Resource
import com.basketballapp.models.Team
import com.basketballapp.util.AbsentLiveData

class SearchViewModel(application: Application, basketballRepository: BasketballRepository)
    : AndroidViewModel(application) {

    val context: Context = application.applicationContext

    private val query = MutableLiveData<String>()
    val resultMatches: LiveData<Resource<List<Match>>> = Transformations.switchMap(query) { q ->
        if (q.isNullOrEmpty()) {
            AbsentLiveData.create()
        } else {
            basketballRepository.searchMatch(q)
        }
    }

    val resultTeams: LiveData<Resource<List<Team>>> = Transformations.switchMap(query) { q ->
        if (q.isNullOrEmpty()) {
            AbsentLiveData.create()
        } else {
            basketballRepository.searchTeam(q)
        }
    }

    fun submitQuery(query: String?) {
        if (!query.isNullOrEmpty() && (this.query.value != query)) {
            this.query.value = query
        }
    }
}