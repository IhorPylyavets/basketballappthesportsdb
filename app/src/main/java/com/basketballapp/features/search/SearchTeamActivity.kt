package com.basketballapp.features.search

import androidx.lifecycle.Observer
import com.basketballapp.custom.BaseSearchActivity
import com.basketballapp.features.main.adapter.TeamAdapter
import com.basketballapp.features.teamdetail.TeamDetailActivity
import com.basketballapp.models.Resource
import com.basketballapp.models.Team
import kotlinx.android.synthetic.main.list_items.*
import com.basketballapp.util.obtainViewModel

class SearchTeamActivity : BaseSearchActivity<Team>(){

    private lateinit var viewModel: SearchViewModel

    override fun setupAdapter() {
        recycler_view.adapter = TeamAdapter(this, Resource.loading(null)) {
            startActivity(TeamDetailActivity.getStartIntent(this, it.idTeam))
        }
    }

    override fun setupData() {
        viewModel = obtainViewModel()
        with (viewModel) {
            submitQuery(query)
            resultTeams.observe(this@SearchTeamActivity, Observer { data -> updateData(data) })
        }
    }

    override fun submitQuery(query: String?) {
        viewModel.submitQuery(query)
    }

    override fun updateData(data: Resource<List<Team>>?) {
        if (data == null || recycler_view == null) return
        (recycler_view.adapter as TeamAdapter).submitData(data)
    }

    private fun obtainViewModel() = obtainViewModel(SearchViewModel::class.java)
}