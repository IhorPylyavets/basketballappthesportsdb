package com.basketballapp.features.matchdetail

import android.app.Application
import android.content.Context
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.basketballapp.data.source.BasketballRepository
import com.basketballapp.models.Match
import com.basketballapp.models.Resource
import com.basketballapp.models.Team
import com.basketballapp.util.AbsentLiveData

class MatchDetailViewModel(context: Application, private val basketballRepository: BasketballRepository)
    : AndroidViewModel(context) {

    private val context: Context = context.applicationContext

    private val idHomeTeam = MutableLiveData<String>()
    private val idAwayTeam = MutableLiveData<String>()
    private val idEvent = MutableLiveData<String>()

    val homeTeam : LiveData<Resource<Team>> = Transformations.switchMap(idHomeTeam) { id ->
        if (id.isNullOrBlank()) {
            AbsentLiveData.create()
        } else {
            basketballRepository.getTeam(id)
        }
    }

    val awayTeam : LiveData<Resource<Team>> = Transformations.switchMap(idAwayTeam) { id ->
        if (id.isNullOrBlank()) {
            AbsentLiveData.create()
        } else {
            basketballRepository.getTeam(id)
        }
    }

    val matchDetail : LiveData<Resource<Match>> = Transformations.switchMap(idEvent) { id ->
        if (id.isNullOrBlank()) {
            AbsentLiveData.create()
        } else {
            basketballRepository.getMatchEventDetail(id)
        }
    }

    val isFavorite : LiveData<Boolean> = Transformations.switchMap(idEvent) { id ->
        basketballRepository.isFavoriteMatch(id)
    }

    fun initData(idEvent: String, idHomeTeam: String, idAwayTeam: String) {
        this.idEvent.value = idEvent
        this.idHomeTeam.value = idHomeTeam
        this.idAwayTeam.value = idAwayTeam
    }

    fun toggleFavorite(matchId: String) {
        isFavorite.value?.let {
            basketballRepository.toggleFavoriteMatch(matchId, it)
        }
    }
}