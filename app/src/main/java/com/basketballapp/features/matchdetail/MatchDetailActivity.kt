package com.basketballapp.features.matchdetail

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import com.basketballapp.R
import com.basketballapp.models.Match
import com.basketballapp.models.Resource
import com.basketballapp.models.Team
import com.google.android.material.snackbar.Snackbar
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_match_detail.*
import com.basketballapp.util.obtainViewModel

class MatchDetailActivity : AppCompatActivity() {

    private lateinit var viewModel: MatchDetailViewModel
    private var menu: Menu? = null

    val idEvent: String by lazy { intent.getStringExtra(ARG_KEY_ID_EVENT) }
    val idHomeTeam: String by lazy { intent.getStringExtra(ARG_KEY_ID_HOME_TEAM) }
    val idAwayTeam: String by lazy { intent.getStringExtra(ARG_KEY_ID_AWAY_TEAM) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_match_detail)

        initData()
        setupToolbar()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        this.menu = menu
        menuInflater.inflate(R.menu.match_detail_menu, menu)
        updateIconIfFavorite(viewModel.isFavorite.value)
        return super.onCreateOptionsMenu(menu)
    }

    private fun initData() {
        viewModel = obtainViewModel()
        viewModel.initData(idEvent, idHomeTeam, idAwayTeam)
        viewModel.homeTeam.observe(this, Observer { res -> setupHomeTeam(res) })
        viewModel.awayTeam.observe(this, Observer { res -> setupAwayTeam(res) })
        viewModel.matchDetail.observe(this, Observer { res -> setupMatchDetail(res) })
        viewModel.isFavorite.observe(this, Observer { isFavorite -> updateFavoriteIcon(isFavorite) })
    }

    private fun setupAwayTeam(resource: Resource<Team>?) {
        resource?.data?.strTeamBadge?.let {
            Picasso.get().load(it).into(iv_club2)
        }
    }

    private fun setupHomeTeam(resource: Resource<Team>?) {
        resource?.data?.strTeamBadge?.let {
            Picasso.get().load(it).into(iv_club1)
        }
    }

    private fun setupMatchDetail(resource: Resource<Match>?) {
        val match = resource?.data ?: return
        with (match) {
            when {
                strThumb.isNullOrEmpty() -> iv_thumb.visibility = View.INVISIBLE
                else -> Picasso.get().load(strThumb).into(iv_thumb)
            }
            tv_date.text = getDate()
            tv_hour.text = getHour()
            tv_club1.text = strHomeTeam
            tv_score1.text = intHomeScore
            tv_club2.text = strAwayTeam
            tv_score2.text = intAwayScore
        }
    }

    private fun updateFavoriteIcon(isFavorite: Boolean?) {
        val menuItem = menu?.findItem(R.id.favorite)
        if (isFavorite == null || menuItem == null) return
        if (isFavorite) {
            menuItem.icon = ContextCompat.getDrawable(this, R.drawable.ic_favorites)
            showMessage("Added to favorites")
        } else {
            menuItem.icon = ContextCompat.getDrawable(this, R.drawable.ic_favorite_border)
            showMessage("Removed from favorites")
        }
    }

    private fun showMessage(message: String) {
        Snackbar.make(content_container, message, Snackbar.LENGTH_SHORT).show()
    }

    private fun setupToolbar() {
        setSupportActionBar(toolbar_detail)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun updateIconIfFavorite(favorite: Boolean?) {
        if (favorite == null) return
        if (favorite) {
            menu?.findItem(R.id.favorite)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_favorites)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            R.id.favorite -> {
                viewModel.toggleFavorite(idEvent)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun obtainViewModel(): MatchDetailViewModel = obtainViewModel(MatchDetailViewModel::class.java)

    companion object {

        private const val ARG_KEY_ID_EVENT = "arg_key_id_event"
        private const val ARG_KEY_ID_HOME_TEAM = "arg_key_id_home_team"
        private const val ARG_KEY_ID_AWAY_TEAM = "arg_key_id_away_team"

        fun getStartIntent(context: Context?, idEvent: String?, idHomeTeam: String?, idAwayTeam: String?): Intent {
            val intent = Intent(context, MatchDetailActivity::class.java)
            intent.putExtra(ARG_KEY_ID_EVENT, idEvent)
            intent.putExtra(ARG_KEY_ID_HOME_TEAM, idHomeTeam)
            intent.putExtra(ARG_KEY_ID_AWAY_TEAM, idAwayTeam)
            return intent
        }
    }
}