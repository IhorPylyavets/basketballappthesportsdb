package com.basketballapp.features.main.fragments

import android.app.SearchManager
import android.content.ComponentName
import android.content.Context
import android.os.Bundle
import android.view.*
import android.widget.AdapterView
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import com.basketballapp.R
import com.basketballapp.features.main.MainActivity
import com.basketballapp.features.main.MatchesViewModel
import com.basketballapp.features.main.adapter.MatchesPagerAdapter
import com.basketballapp.features.search.SearchMatchActivity
import kotlinx.android.synthetic.main.fragment_matches.*
import kotlinx.android.synthetic.main.fragment_matches.view.*

class MatchesFragment : Fragment(), AdapterView.OnItemSelectedListener {

    private lateinit var viewModel: MatchesViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_matches, container, false).also {
            viewModel = (activity as MainActivity).obtainViewModel()
            it.spinner_leagues.onItemSelectedListener = this
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setHasOptionsMenu(true)
        setupViewPager()
    }

    private fun setupViewPager() {
        view_pager_matches.adapter = MatchesPagerAdapter(childFragmentManager)
        tab_layout_matches.setupWithViewPager(view_pager_matches)
    }

    override fun onNothingSelected(p0: AdapterView<*>?) { }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        viewModel.setMatchesFilterBy(position)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater?.inflate(R.menu.main_menu, menu)

        var  searchManager = context?.getSystemService(Context.SEARCH_SERVICE) as SearchManager
        (menu?.findItem(R.id.menu_search)?.actionView as SearchView).apply {
            setSearchableInfo(searchManager.getSearchableInfo(ComponentName(context, SearchMatchActivity::class.java)))
        }
    }

    companion object {
        fun newInstance() = MatchesFragment()
    }
}