package com.basketballapp.features.main.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.basketballapp.features.main.fragments.FavoritesFragment
import com.basketballapp.features.main.fragments.MatchesFragment
import com.basketballapp.features.main.fragments.TeamsFragment

class MainPagerAdapter(fragmentManager: FragmentManager?) : FragmentPagerAdapter(fragmentManager) {
    override fun getItem(position: Int): Fragment {
        return when(position) {
            0 -> MatchesFragment.newInstance()
            1 -> TeamsFragment.newInstance()
            2 -> FavoritesFragment.newInstance()
            else -> error("Cannot place more than 3 fragments")
        }
    }

    override fun getCount(): Int = 3
}