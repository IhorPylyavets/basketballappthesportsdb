package com.basketballapp.features.main.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.basketballapp.R
import com.basketballapp.features.main.MainActivity
import com.basketballapp.features.main.MatchesViewModel
import com.basketballapp.features.main.adapter.MatchesAdapter
import com.basketballapp.features.matchdetail.MatchDetailActivity
import com.basketballapp.models.Match
import com.basketballapp.models.Resource
import com.basketballapp.models.Status
import com.basketballapp.models.Team
import kotlinx.android.synthetic.main.list_items.*

class ListMatchesFragment : Fragment(){

    private lateinit var viewModel: MatchesViewModel

    companion object {
        private const val KEY_MATCH = "key_match"
        const val TYPE_NEXT_MATCH = "type_next_match"
        const val TYPE_PREV_MATCH = "type_prev_match"

        fun newInstance(type: String) : ListMatchesFragment {
            val fragment = ListMatchesFragment()
            fragment.arguments = Bundle().apply {
                putString(KEY_MATCH, type)
            }

            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.list_items, container, false).also {
            viewModel = (activity as MainActivity).obtainViewModel()
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupList()
    }

    private fun setupList() {
        swipe_refresh_layout.setOnRefreshListener { viewModel.refreshMatches() }
        recycler_view.layoutManager = LinearLayoutManager(context)
        recycler_view.adapter = MatchesAdapter(context, Resource.loading(null)) {
            context?.startActivity(
                MatchDetailActivity.getStartIntent(context, it.idEvent, it.idHomeTeam, it.idAwayTeam)
            )
        }

        when (getType()) {
            TYPE_NEXT_MATCH -> {
                viewModel.nextMatches.observe(activity as MainActivity, Observer { data ->
                    updateData(data)
                })
            }
            TYPE_PREV_MATCH -> {
                viewModel.prevMatch.observe(activity as MainActivity, Observer { data ->
                    updateData(data)
                })
            }
        }
    }

    private fun getType(): String? {
        return arguments?.getString(KEY_MATCH)
    }

    private fun updateData(data: Resource<List<Match>>?) {
        if (data == null || recycler_view == null) return
        viewModel.teams.observe(activity as MainActivity, Observer { teams ->
            data.data?.forEach { match ->
                run {
                    match.strHomeTeamIcon = match.idHomeTeam?.let { it ->
                        teams.data?.let { team -> findTeamBadge(it, team) }
                    }
                    match.strAwayTeamIcon = match.idAwayTeam?.let { it ->
                        teams.data?.let { team -> findTeamBadge(it, team) }
                    }
                }
            }

            (recycler_view.adapter as MatchesAdapter).submitData(data)
            updateRefreshIndicator(data)
        })
    }

    private fun <T> updateRefreshIndicator(data: Resource<List<T>>) {
        swipe_refresh_layout.isRefreshing = data.status == Status.LOADING
    }
}