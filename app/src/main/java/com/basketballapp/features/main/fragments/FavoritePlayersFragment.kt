package com.basketballapp.features.main.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.basketballapp.R
import com.basketballapp.features.main.MainActivity
import com.basketballapp.features.main.MatchesViewModel
import com.basketballapp.features.main.adapter.PlayersAdapter
import com.basketballapp.features.playerdetail.PlayerDetailActivity
import com.basketballapp.models.Player
import com.basketballapp.models.Resource
import kotlinx.android.synthetic.main.list_items.*

class FavoritePlayersFragment : Fragment() {

    private lateinit var viewModel: MatchesViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.list_items, container, false).also {
            viewModel = (activity as MainActivity).obtainViewModel()
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        setupList()
        initData()
    }

    private fun setupList() {
        swipe_refresh_layout.isEnabled = false
        recycler_view.layoutManager = LinearLayoutManager(context)
        recycler_view.adapter = PlayersAdapter(context, Resource.loading(null)) {
            startActivity(PlayerDetailActivity.getStartIntent(context, it.idPlayer))
        }
    }

    private fun initData() {
        viewModel.favoritePlayers.observe(activity as MainActivity, Observer { res ->
            updateData(res)
        })
    }

    private fun updateData(res: Resource<List<Player>>?) {
        if (recycler_view == null || res == null) return
        (recycler_view.adapter as PlayersAdapter).submitData(res)
    }

    companion object {
        fun newInstance() = FavoritePlayersFragment()
    }
}