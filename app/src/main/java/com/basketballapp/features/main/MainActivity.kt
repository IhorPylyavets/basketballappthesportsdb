package com.basketballapp.features.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import com.basketballapp.R
import com.basketballapp.features.main.adapter.MainPagerAdapter
import kotlinx.android.synthetic.main.activity_main.*
import com.basketballapp.util.obtainViewModel

class MainActivity : AppCompatActivity() {

    lateinit var viewModel: MatchesViewModel
    var viewModelFactory: ViewModelProvider.Factory? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(toolbar_main)

        view_pager_main.adapter = MainPagerAdapter(supportFragmentManager)
        bottom_navigation_view_main.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.matches -> view_pager_main.setCurrentItem(0, false)
                R.id.teams -> view_pager_main.setCurrentItem(1, false)
                R.id.favorites -> view_pager_main.setCurrentItem(2, false)
            }
            true
        }

        viewModel = obtainViewModel()
    }

    fun obtainViewModel(): MatchesViewModel = obtainViewModel(MatchesViewModel::class.java, viewModelFactory)
}
