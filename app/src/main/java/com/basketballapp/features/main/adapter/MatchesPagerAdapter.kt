package com.basketballapp.features.main.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.basketballapp.features.main.fragments.ListMatchesFragment

class MatchesPagerAdapter(fragmentManager: FragmentManager?) : FragmentPagerAdapter(fragmentManager) {
    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> ListMatchesFragment.newInstance(ListMatchesFragment.TYPE_NEXT_MATCH)
            1 -> ListMatchesFragment.newInstance(ListMatchesFragment.TYPE_PREV_MATCH)
            else -> error("Cannot create more than 2 fragment")
        }
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when (position) {
            0 -> "Next"
            1 -> "Last"
            else -> super.getPageTitle(position)
        }
    }

    override fun getCount(): Int = 2
}