package com.basketballapp.features.main.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.basketballapp.R
import com.basketballapp.features.main.adapter.FavoritesPagerAdapter
import kotlinx.android.synthetic.main.fragment_favorites.*

class FavoritesFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_favorites, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupViewPager()
    }

    private fun setupViewPager() {
        view_pager_favorites.adapter = FavoritesPagerAdapter(childFragmentManager)
        tab_layout_favorites.setupWithViewPager(view_pager_favorites)
    }

    companion object {
        fun newInstance() = FavoritesFragment()
    }
}