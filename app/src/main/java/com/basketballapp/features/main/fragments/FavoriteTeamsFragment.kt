package com.basketballapp.features.main.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.basketballapp.R
import com.basketballapp.features.main.MainActivity
import com.basketballapp.features.main.MatchesViewModel
import com.basketballapp.features.main.adapter.TeamsAdapter
import com.basketballapp.features.teamdetail.TeamDetailActivity
import com.basketballapp.models.Resource
import com.basketballapp.models.Team
import kotlinx.android.synthetic.main.list_items.*

class FavoriteTeamsFragment: Fragment() {

    private lateinit var viewModel: MatchesViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.list_items, container, false).also {
            viewModel = (activity as MainActivity).obtainViewModel()
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        setupList()
        initData()
    }

    private fun setupList() {
        swipe_refresh_layout.isEnabled = false
        recycler_view.layoutManager = LinearLayoutManager(context)
        recycler_view.adapter = TeamsAdapter(context, Resource.loading(null)) {
            startActivity(TeamDetailActivity.getStartIntent(context, it.idTeam))
        }
    }

    private fun initData() {
        viewModel.favoriteTeams.observe(activity as MainActivity, Observer { res ->
            updateData(res)
        })
    }

    private fun updateData(res: Resource<List<Team>>?) {
        if (recycler_view == null || res == null) return
        (recycler_view.adapter as TeamsAdapter).submitData(res)
    }

    companion object {
        fun newInstance() = FavoriteTeamsFragment()
    }
}