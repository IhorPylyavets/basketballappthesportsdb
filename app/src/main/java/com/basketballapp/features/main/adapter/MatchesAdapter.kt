package com.basketballapp.features.main.adapter

import android.animation.AnimatorInflater
import android.content.Context
import android.content.Intent
import android.os.Build
import android.provider.CalendarContract
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.basketballapp.R
import com.basketballapp.features.base.BaseAdapter
import com.basketballapp.features.main.fragments.ListMatchesFragment
import com.basketballapp.models.Match
import com.basketballapp.models.Resource
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_match.view.*

class MatchesAdapter(context: Context?,
                     resource: Resource<List<Match>>,
                     private val clickListener: (Match) -> Unit) : BaseAdapter<Match>(context, resource) {

    override fun createDataViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        return MatchItem(LayoutInflater.from(parent.context).inflate(R.layout.item_match, parent, false))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is MatchItem) {
            holder.bind(resource.data?.get(position), clickListener)
        }
    }

    inner class MatchItem(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(match: Match?, clickListener: (Match) -> Unit) {
            match?.let { match ->
                with(itemView) {
                    tv_hour.text = match.getHour()
                    tv_date.text = match.getDate()
                    tv_club1.text = match.strHomeTeam
                    tv_score1.text = match.intHomeScore
                    tv_club2.text = match.strAwayTeam
                    tv_score2.text = match.intAwayScore
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        itemView.stateListAnimator =
                            AnimatorInflater.loadStateListAnimator(this.context, R.animator.lift_on_touch)
                    }
                    if (match.matchType == ListMatchesFragment.TYPE_NEXT_MATCH) {
                        iv_notification.visibility = View.VISIBLE
                        iv_notification.setOnClickListener {
                            context?.startActivity(Intent(Intent.ACTION_INSERT).apply {
                                data = CalendarContract.Events.CONTENT_URI
                                putExtra(CalendarContract.Events.TITLE, "${match.strHomeTeam} vs ${match.strAwayTeam}")
                                putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, match.getStartTime())
                            })
                        }
                    }

                    if (!match.strHomeTeamIcon.isNullOrEmpty()) { Picasso.get().load(match.strHomeTeamIcon).into(iv_home_team) }
                    if (!match.strAwayTeamIcon.isNullOrEmpty()) { Picasso.get().load(match.strAwayTeamIcon).into(iv_away_team) }

                    setOnClickListener {
                        clickListener(match)
                    }
                }
            }
        }
    }
}