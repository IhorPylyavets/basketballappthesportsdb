package com.basketballapp.features.main.fragments

import com.basketballapp.models.Team

fun findTeamBadge(idTeam: String, teams: List<Team>): String? {
    var teamBadge = ""
    teams.forEach {
        if (it.idTeam == idTeam) teamBadge = it.strTeamBadge.toString()
    }
    return teamBadge
}