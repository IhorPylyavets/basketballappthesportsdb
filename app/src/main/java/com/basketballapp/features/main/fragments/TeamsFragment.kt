package com.basketballapp.features.main.fragments

import android.app.SearchManager
import android.content.ComponentName
import android.content.Context
import android.os.Bundle
import android.view.*
import android.widget.AdapterView
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.basketballapp.R
import com.basketballapp.features.main.MainActivity
import com.basketballapp.features.main.MatchesViewModel
import com.basketballapp.features.main.adapter.TeamAdapter
import com.basketballapp.features.search.SearchTeamActivity
import com.basketballapp.features.teamdetail.TeamDetailActivity
import com.basketballapp.models.Resource
import com.basketballapp.models.Status
import kotlinx.android.synthetic.main.fragment_teams.view.*
import kotlinx.android.synthetic.main.list_items.*

class TeamsFragment : Fragment(), AdapterView.OnItemSelectedListener {

    private lateinit var viewModel: MatchesViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_teams, container, false).also {
            viewModel = (activity as MainActivity).obtainViewModel()
            it.spinner_leagues.onItemSelectedListener = this
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setHasOptionsMenu(true)
        setupList()
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater?.inflate(R.menu.main_menu, menu)

        val searchManager = context?.getSystemService(Context.SEARCH_SERVICE) as SearchManager
        (menu?.findItem(R.id.menu_search)?.actionView as SearchView).apply {
            setSearchableInfo(searchManager.getSearchableInfo(ComponentName(context, SearchTeamActivity::class.java)))
        }
    }

    private fun setupList() {
        swipe_refresh_layout.setOnRefreshListener { viewModel.refreshTeams() }
        recycler_view.layoutManager = LinearLayoutManager(context)
        recycler_view.adapter = TeamAdapter(context, Resource.loading(null)) {
            startActivity(TeamDetailActivity.getStartIntent(context, it.idTeam))
        }
        viewModel.teams.observe(this, Observer { data ->
            (recycler_view.adapter as TeamAdapter).submitData(data)
            updateRefreshIndicator(data)
        })
    }

    private fun <T> updateRefreshIndicator(data: Resource<List<T>>) {
        swipe_refresh_layout.isRefreshing = data.status == Status.LOADING
    }

    override fun onNothingSelected(p0: AdapterView<*>?) { }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        viewModel.setTeamFilterBy(position)
    }

    companion object {
        fun newInstance() = TeamsFragment()
    }
}