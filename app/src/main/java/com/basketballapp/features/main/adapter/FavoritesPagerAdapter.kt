package com.basketballapp.features.main.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.basketballapp.features.main.fragments.FavoriteMatchesFragment
import com.basketballapp.features.main.fragments.FavoritePlayersFragment
import com.basketballapp.features.main.fragments.FavoriteTeamsFragment

class FavoritesPagerAdapter(fragmentManager: FragmentManager?) : FragmentPagerAdapter(fragmentManager) {
    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> FavoriteMatchesFragment.newInstance()
            1 -> FavoriteTeamsFragment.newInstance()
            2 -> FavoritePlayersFragment.newInstance()
            else -> error("Cannot create more than 3 fragment")
        }
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when (position) {
            0 -> "Matches"
            1 -> "Teams"
            2 -> "Players"
            else -> super.getPageTitle(position)
        }
    }

    override fun getCount(): Int = 3
}