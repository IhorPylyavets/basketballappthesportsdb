package com.basketballapp.features.main

import android.app.Application
import android.content.Context
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.basketballapp.data.source.BasketballRepository
import com.basketballapp.models.Match
import com.basketballapp.models.Player
import com.basketballapp.models.Resource
import com.basketballapp.models.Team
import com.basketballapp.util.AbsentLiveData
import com.basketballapp.util.getLeaguesId

class MatchesViewModel(context: Application, basketballRepository: BasketballRepository)
    : AndroidViewModel(context) {

    val matchFilterId = MutableLiveData<String>()
    val teamFilterId = MutableLiveData<String>()

    val context: Context = context.applicationContext

    fun setMatchesFilterBy(position: Int) {
        matchFilterId.value = context.getLeaguesId(position)
    }

    fun setTeamFilterBy(position: Int) {
        teamFilterId.value = context.getLeaguesId(position)
    }

    fun refreshMatches() {
        matchFilterId?.value?.let {
            matchFilterId.value = it
        }
    }

    val nextMatches: LiveData<Resource<List<Match>>> = Transformations.switchMap(matchFilterId)
    { leagueId ->
        if (leagueId.isNullOrBlank()) {
            AbsentLiveData.create()
        } else {
            basketballRepository.nextMatches(leagueId)
        }
    }

    val prevMatch: LiveData<Resource<List<Match>>> = Transformations.switchMap(matchFilterId)
    { leagueId ->
        if (leagueId.isNullOrBlank()) {
            AbsentLiveData.create()
        } else {
            basketballRepository.prevMatches(leagueId)
        }
    }

    fun refreshTeams() {
        teamFilterId.value?.let {
            teamFilterId.value = it
        }
    }

    val teams: LiveData<Resource<List<Team>>> = Transformations.switchMap(teamFilterId) { leagueId ->
        if (leagueId.isNullOrBlank()) {
            AbsentLiveData.create()
        } else {
            basketballRepository.teams(leagueId)
        }
    }

    val favoriteMatches: LiveData<Resource<List<Match>>> = basketballRepository.getFavoriteMatches()

    val favoriteTeams: LiveData<Resource<List<Team>>> = basketballRepository.getFavoriteTeams()

    val favoritePlayers: LiveData<Resource<List<Player>>> = basketballRepository.getFavoritePlayers()
}