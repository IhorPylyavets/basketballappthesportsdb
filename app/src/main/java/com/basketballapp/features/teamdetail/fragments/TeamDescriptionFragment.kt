package com.basketballapp.features.teamdetail.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.basketballapp.R
import com.basketballapp.features.teamdetail.TeamDetailActivity
import com.basketballapp.features.teamdetail.TeamDetailViewModel
import com.basketballapp.models.Resource
import com.basketballapp.models.Team
import kotlinx.android.synthetic.main.fragment_team_detail.*

class TeamDescriptionFragment : Fragment() {

    private lateinit var viewModel: TeamDetailViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return LayoutInflater.from(context).inflate(R.layout.fragment_team_detail, container, false).also {
            viewModel = (activity as TeamDetailActivity).obtainViewModel()
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.team.observe(this, Observer { res ->
            updateTeamDetail(res)
        })
    }

    private fun updateTeamDetail(resource: Resource<Team>?) {
        val team = resource?.data ?: return
        tv_desc.text = team.strDescriptionEN
    }

    companion object {
        fun newInstance() = TeamDescriptionFragment()
    }
}