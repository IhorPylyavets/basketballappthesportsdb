package com.basketballapp.features.teamdetail.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.basketballapp.R
import com.basketballapp.features.playerdetail.PlayerDetailActivity
import com.basketballapp.features.teamdetail.TeamDetailActivity
import com.basketballapp.features.teamdetail.TeamDetailViewModel
import com.basketballapp.features.teamdetail.adapter.PlayerAdapter
import com.basketballapp.models.Resource
import kotlinx.android.synthetic.main.list_items.*

class TeamPlayerFragment : Fragment() {

    private lateinit var viewModel: TeamDetailViewModel
    private val teamId: String? by lazy { arguments?.getString(ARG_KEY_TEAM_ID) }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return LayoutInflater.from(context).inflate(R.layout.list_items, container, false).also {
            viewModel = (activity as TeamDetailActivity).obtainViewModel()
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        initData()
        setupList()
    }

    private fun initData() {
        viewModel.players.observe(this, Observer { res -> (recycler_view.adapter as PlayerAdapter).submitData(res) })
    }

    private fun setupList() {
        swipe_refresh_layout.isEnabled = false
        recycler_view.layoutManager = LinearLayoutManager(context)
        recycler_view.adapter = PlayerAdapter(context, Resource.loading(null)) {
            startActivity(PlayerDetailActivity.getStartIntent(context, it.idPlayer))
        }
    }

    companion object {

        private const val ARG_KEY_TEAM_ID = "arg_key_team_id"

        fun newInstance(teamId: String) = TeamPlayerFragment().apply {
            val bundle = Bundle()
            bundle.putString(ARG_KEY_TEAM_ID, teamId)
            arguments = bundle
        }
    }
}