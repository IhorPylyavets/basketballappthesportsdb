package com.basketballapp.features.teamdetail.adapter

import android.animation.AnimatorInflater
import android.content.Context
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.basketballapp.R
import com.basketballapp.features.base.BaseAdapter
import com.basketballapp.models.Player
import com.basketballapp.models.Resource
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_player.view.*

class PlayerAdapter(context: Context?, resource: Resource<List<Player>>, val clickListener: (Player) -> Unit) :
    BaseAdapter<Player>(context, resource) {
    override fun createDataViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        return PlayerItem(LayoutInflater.from(context).inflate(R.layout.item_player, parent, false))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is PlayerItem) {
            holder.bind(resource.data?.get(position), clickListener)
        }
    }

    inner class PlayerItem(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(res: Player?, clickListener: (Player) -> Unit) {
            res ?: return
            with(itemView) {
                if (!res.strCutout.isNullOrBlank()) {
                    Picasso.get().load(res.strCutout).into(ivPhoto)
                } else {
                    Picasso.get().load(res.strThumb).into(ivPhoto)
                }
                tvName.text = res.strPlayer
                tvRole.text = res.strPosition

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    stateListAnimator = AnimatorInflater.loadStateListAnimator(this.context, R.animator.lift_on_touch)
                }

                setOnClickListener { clickListener(res) }
            }
        }
    }
}