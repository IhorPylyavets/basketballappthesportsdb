package com.basketballapp.features.teamdetail.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.basketballapp.features.teamdetail.fragments.TeamDescriptionFragment
import com.basketballapp.features.teamdetail.fragments.TeamPlayerFragment

class TeamDetailPagerAdapter(fragmentManager: FragmentManager?, private val teamId: String)
    : FragmentPagerAdapter(fragmentManager) {

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> TeamDescriptionFragment.newInstance()
            1 -> TeamPlayerFragment.newInstance(teamId)
            else -> error("Can't have more than 2 fragment on team detail")
        }
    }

    override fun getCount(): Int = 2

    override fun getPageTitle(position: Int): CharSequence? {
        return when (position) {
            0 -> "OVERVIEW"
            1 -> "PLAYERS"
            else -> super.getPageTitle(position)
        }
    }
}