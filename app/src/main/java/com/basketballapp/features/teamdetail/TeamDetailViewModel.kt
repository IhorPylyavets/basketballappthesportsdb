package com.basketballapp.features.teamdetail

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.basketballapp.data.source.BasketballRepository
import com.basketballapp.models.Player
import com.basketballapp.models.Resource
import com.basketballapp.models.Team
import com.basketballapp.util.AbsentLiveData

class TeamDetailViewModel(context: Application, private val basketballRepository: BasketballRepository)
    : AndroidViewModel(context) {

    private val teamId = MutableLiveData<String>()

    fun initData(teamId: String) {
        this.teamId.value = teamId
    }


    fun toggleFavorite(matchId: String) {
        isFavorite.value?.let {
            basketballRepository.toggleFavoriteTeam(matchId, it)
        }
    }

    val isFavorite: LiveData<Boolean> = Transformations.switchMap(teamId) { id ->
        basketballRepository.isFavoriteTeam(id)
    }

    val team: LiveData<Resource<Team>> = Transformations.switchMap(teamId) { id ->
        if (id.isNullOrBlank()) {
            AbsentLiveData.create()
        } else {
            basketballRepository.getTeam(id)
        }
    }

    val players: LiveData<Resource<List<Player>>> = Transformations.switchMap(teamId) { id ->
        if (id.isNullOrBlank()) {
            AbsentLiveData.create()
        } else {
            basketballRepository.getPlayers(id)
        }
    }
}