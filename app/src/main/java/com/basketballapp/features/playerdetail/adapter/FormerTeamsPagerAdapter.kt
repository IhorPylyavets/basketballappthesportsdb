package com.basketballapp.features.playerdetail.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.basketballapp.features.playerdetail.fragments.PlayerFormerTeamsFragment
import com.basketballapp.features.playerdetail.fragments.PlayerInfoFragment

class FormerTeamsPagerAdapter(fragmentManager: FragmentManager?, private val playerId: String)
    : FragmentPagerAdapter(fragmentManager) {

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> PlayerInfoFragment.newInstance()
            1 -> PlayerFormerTeamsFragment.newInstance(playerId)
            else -> error("Can't have more than 2 fragment on team detail")
        }
    }

    override fun getCount(): Int = 2

    override fun getPageTitle(position: Int): CharSequence? {
        return when (position) {
            0 -> "INFO"
            1 -> "FORMER TEAMS"
            else -> super.getPageTitle(position)
        }
    }
}