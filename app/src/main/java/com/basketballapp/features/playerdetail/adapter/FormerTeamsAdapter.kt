package com.basketballapp.features.playerdetail.adapter

import android.animation.AnimatorInflater
import android.content.Context
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.basketballapp.R
import com.basketballapp.features.base.BaseAdapter
import com.basketballapp.models.FormerTeam
import com.basketballapp.models.Resource
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_former_team.view.*

class FormerTeamsAdapter(context: Context?, resource: Resource<List<FormerTeam>>) : BaseAdapter<FormerTeam>(context, resource) {

    override fun createDataViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        return PlayerItem(LayoutInflater.from(context).inflate(R.layout.item_former_team, parent, false))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is PlayerItem) {
            holder.bind(resource.data?.get(position))
        }
    }

    inner class PlayerItem(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(formerTeam: FormerTeam?) {
            formerTeam ?: return
            with(itemView) {
                if (!formerTeam.strTeamBadge.isNullOrEmpty()) { Picasso.get().load(formerTeam.strTeamBadge).into(iv_club) }
                tv_club.text = formerTeam.strFormerTeam
                tv_joined.text = formerTeam.strJoined
                tv_departed.text = formerTeam.strDeparted

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    stateListAnimator = AnimatorInflater.loadStateListAnimator(this.context, R.animator.lift_on_touch)
                }
            }
        }
    }
}