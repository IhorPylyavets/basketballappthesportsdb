package com.basketballapp.features.playerdetail

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import com.basketballapp.R
import com.basketballapp.features.playerdetail.adapter.FormerTeamsPagerAdapter
import com.basketballapp.models.Player
import com.basketballapp.models.Resource
import kotlinx.android.synthetic.main.activity_player_detail.*
import com.basketballapp.util.obtainViewModel
import com.google.android.material.snackbar.Snackbar
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_player_detail.content_container
import kotlinx.android.synthetic.main.activity_player_detail.ivArt
import kotlinx.android.synthetic.main.activity_player_detail.tabPlayer
import kotlinx.android.synthetic.main.activity_player_detail.toolbar_player_detail
import kotlinx.android.synthetic.main.activity_player_detail.tvHeight
import kotlinx.android.synthetic.main.activity_player_detail.tvWeight

class PlayerDetailActivity : AppCompatActivity() {

    private lateinit var viewModel: PlayerDetailViewModel
    private val playerId: String by lazy { intent.getStringExtra(ARG_KEY_PLAYER_ID) }

    private var menu: Menu? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_player_detail)

        initToolbar()
        initData()
        setupData()
        setupPager()
    }

    private fun initToolbar() {
        setSupportActionBar(toolbar_player_detail)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun initData() {
        viewModel = obtainViewModel()
        viewModel.initData(playerId)
        viewModel.player.observe(this, Observer { res -> updateData(res) })
    }

    private fun updateData(res: Resource<Player>?) {
        if (res?.data == null) return
        val player = res.data

        supportActionBar?.title = player.strPlayer

        with(player) {
            if (!player.strCutout.isNullOrBlank()) {
                Picasso.get().load(player.strCutout).into(ivArt)
            } else {
                Picasso.get().load(player.strThumb).into(ivArt)
            }
            tvWeight.text = getWeight()
            tvHeight.text = getHeight()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        this.menu = menu
        menuInflater.inflate(R.menu.player_detail_menu, menu)
        updateIconIfFavorite(viewModel.isFavorite.value)
        return super.onCreateOptionsMenu(menu)
    }

    private fun updateIconIfFavorite(favorite: Boolean?) {
        if (favorite == null) return
        if (favorite) {
            menu?.findItem(R.id.favorite)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_favorites)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            R.id.favorite -> {
                viewModel.toggleFavorite(playerId)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun setupData() {
        viewModel = obtainViewModel().apply {
            initData(playerId)
            player.observe(this@PlayerDetailActivity, Observer { res -> updatePlayerDetail(res) })
            isFavorite.observe(this@PlayerDetailActivity, Observer { isFavorite -> updateFavoriteIcon(isFavorite) })
        }
    }

    private fun setupPager() {
        view_pager_player_detail.adapter = FormerTeamsPagerAdapter(supportFragmentManager, playerId)
        tabPlayer.setupWithViewPager(view_pager_player_detail)
    }

    private fun updatePlayerDetail(resource: Resource<Player>?) {
        val player = resource?.data ?: return
        with(player) {
            supportActionBar?.title = strPlayer
        }
    }

    private fun updateFavoriteIcon(isFavorite: Boolean?) {
        val menuItem = menu?.findItem(R.id.favorite)
        if (isFavorite == null || menuItem == null) return
        if (isFavorite) {
            menuItem.icon = ContextCompat.getDrawable(this, R.drawable.ic_favorites)
            showMessage("Added to favorites")
        } else {
            menuItem.icon = ContextCompat.getDrawable(this, R.drawable.ic_favorite_border)
            showMessage("Removed from favorites")
        }
    }

    private fun showMessage(message: String) {
        Snackbar.make(content_container, message, Snackbar.LENGTH_SHORT).show()
    }

    fun obtainViewModel(): PlayerDetailViewModel = obtainViewModel(PlayerDetailViewModel::class.java)

    companion object {

        private const val ARG_KEY_PLAYER_ID = "arg_key_player_id"

        fun getStartIntent(context: Context?, playerId: String): Intent =
            Intent(context, PlayerDetailActivity::class.java).apply {
                putExtra(ARG_KEY_PLAYER_ID, playerId)
            }
    }
}