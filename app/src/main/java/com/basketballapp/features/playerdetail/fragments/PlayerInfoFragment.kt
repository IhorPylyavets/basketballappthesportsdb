package com.basketballapp.features.playerdetail.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.basketballapp.R
import com.basketballapp.features.playerdetail.PlayerDetailActivity
import com.basketballapp.features.playerdetail.PlayerDetailViewModel
import com.basketballapp.models.Player
import com.basketballapp.models.Resource
import kotlinx.android.synthetic.main.fragment_player_info.*

class PlayerInfoFragment : Fragment() {

    private lateinit var viewModel: PlayerDetailViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return LayoutInflater.from(context).inflate(R.layout.fragment_player_info, container, false).also {
            viewModel = (activity as PlayerDetailActivity).obtainViewModel()
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.player.observe(this, Observer { res ->
            updateTeamDetail(res)
        })
    }

    private fun updateTeamDetail(resource: Resource<Player>?) {
        val player = resource?.data ?: return
        tvRole.text = player.strPosition
        tvDesc.text = player.strDescriptionEN
    }

    companion object {
        fun newInstance() = PlayerInfoFragment()
    }
}