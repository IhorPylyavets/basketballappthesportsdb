package com.basketballapp.features.playerdetail

import android.app.Application
import android.content.Context
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.basketballapp.data.source.BasketballRepository
import com.basketballapp.util.AbsentLiveData

class PlayerDetailViewModel(context: Application, private val basketballRepository: BasketballRepository)
    : AndroidViewModel(context) {

    val context: Context = context.applicationContext

    private val playerId = MutableLiveData<String>()

    val player = Transformations.switchMap(playerId) { id ->
        if (id.isNullOrBlank()) {
            AbsentLiveData.create()
        } else {
            basketballRepository.getPlayer(id)
        }
    }

    fun initData(playerId : String) {
        this.playerId.value = playerId
    }

    val isFavorite: LiveData<Boolean> = Transformations.switchMap(playerId) { id ->
        basketballRepository.isFavoritePlayer(id)
    }

    fun toggleFavorite(matchId: String) {
        isFavorite.value?.let {
            basketballRepository.toggleFavoritePlayer(matchId, it)
        }
    }

    val formerTeams = Transformations.switchMap(playerId) { id ->
        if (id.isNullOrBlank()) {
            AbsentLiveData.create()
        } else {
            basketballRepository.getFormerTeams(id)
        }
    }
}