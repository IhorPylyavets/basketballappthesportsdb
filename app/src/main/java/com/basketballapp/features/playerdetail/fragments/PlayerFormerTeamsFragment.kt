package com.basketballapp.features.playerdetail.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.basketballapp.R
import com.basketballapp.features.playerdetail.PlayerDetailActivity
import com.basketballapp.features.playerdetail.PlayerDetailViewModel
import com.basketballapp.features.playerdetail.adapter.FormerTeamsAdapter
import com.basketballapp.models.Resource
import kotlinx.android.synthetic.main.list_items.*

class PlayerFormerTeamsFragment : Fragment() {

    private lateinit var viewModel: PlayerDetailViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return LayoutInflater.from(context).inflate(R.layout.list_items, container, false).also {
            viewModel = (activity as PlayerDetailActivity).obtainViewModel()
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        initData()
        setupList()
    }

    private fun initData() {
        viewModel.formerTeams.observe(this, Observer { res -> (recycler_view.adapter as FormerTeamsAdapter).submitData(res) })
    }

    private fun setupList() {
        swipe_refresh_layout.isEnabled = false
        recycler_view.layoutManager = LinearLayoutManager(context)
        recycler_view.adapter = FormerTeamsAdapter(context, Resource.loading(null))
    }

    companion object {

        private const val ARG_KEY_PLAYER_ID = "arg_key_player_id"

        fun newInstance(teamId: String) = PlayerFormerTeamsFragment().apply {
            val bundle = Bundle()
            bundle.putString(ARG_KEY_PLAYER_ID, teamId)
            arguments = bundle
        }
    }
}