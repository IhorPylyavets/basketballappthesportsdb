package com.basketballapp

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.basketballapp.data.source.BasketballRepository
import com.basketballapp.data.source.local.BasketballDatabase
import com.basketballapp.data.source.remote.BasketballServiceFactory
import com.basketballapp.features.main.MatchesViewModel
import com.basketballapp.features.matchdetail.MatchDetailViewModel
import com.basketballapp.features.playerdetail.PlayerDetailViewModel
import com.basketballapp.features.search.SearchViewModel
import com.basketballapp.features.teamdetail.TeamDetailViewModel

class ViewModelFactory private constructor(
    private val app: Application,
    private val repository: BasketballRepository
) : ViewModelProvider.NewInstanceFactory() {

    companion object {
        @Volatile
        private var INSTANCE: ViewModelFactory? = null

        fun getInstance(application: Application): ViewModelFactory {
            return INSTANCE ?: synchronized(ViewModelFactory::class.java) {
                ViewModelFactory(
                    application,
                    BasketballRepository.getInstance(
                        BasketballDatabase.getDatabase(application.applicationContext),
                        BasketballServiceFactory.getService()
                    )
                ).also { INSTANCE = it }
            }
        }
    }

    override fun <T : ViewModel?> create(modelClass: Class<T>): T =
        with(modelClass) {
            when {
                isAssignableFrom(MatchesViewModel::class.java) -> MatchesViewModel(app, repository)
                isAssignableFrom(MatchDetailViewModel::class.java) -> MatchDetailViewModel(app, repository)
                isAssignableFrom(TeamDetailViewModel::class.java) -> TeamDetailViewModel(app, repository)
                isAssignableFrom(PlayerDetailViewModel::class.java) -> PlayerDetailViewModel(app, repository)
                isAssignableFrom(SearchViewModel::class.java) -> SearchViewModel(app, repository)
                else -> error("Invalid View Model class")
            }
        } as T

}